const axios = require("axios");
require("dotenv").config();
const hmacSha256 = require("crypto-js/hmac-sha256");
const querystring = require("querystring");
const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");

const packageDefinition = protoLoader.loadSync("./trade.proto", {
  keepcase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
});
const protoDescriptor = grpc.loadPackageDefinition(packageDefinition);
const executor = protoDescriptor.Executor;
const NOTIFY_URL = "https://notify-api.line.me/api/notify";

const instance = axios.create({
  baseURL: "https://api.binance.com",
  headers: {
    "X-MBX-APIKEY": process.env.API_KEY,
    "content-type": "application/x-www-form-urlencoded",
  },
});
const tradingCoins = {
  DOTUSDT: {},
};

instance
  .get("/api/v3/exchangeInfo")
  .then((res) => {
    res.data.symbols
      .filter((e) => e.symbol in tradingCoins)
      .forEach((e) => {
        tradingCoins[e.symbol] = e;
      });
  })
  .catch((e) => {
    console.log(e);
  });

async function buy(call, callback) {
  const ticker = call.request.ticker.toUpperCase();
  const coin = tradingCoins[ticker].baseAsset;
  const quote = tradingCoins[ticker].quoteAsset;

  try {
    const quoteQuantity = await getQuantity(quote);
    const res = await instance.post(
      "/api/v3/order",
      qsWithSignature({
        symbol: ticker,
        side: "BUY",
        type: "MARKET",
        quoteOrderQty: quoteQuantity / 2,
        timestamp: Date.now(),
      }),
    );

    const resMessages = mapOrders(coin, res.data.fills, "BOUGHT");
    sendNotification(resMessages);
    console.log(resMessages);

    callback(null, { messages: resMessages });
  } catch (e) {
    sendNotification(e.toString());
    console.log(e);
    callback(null, { messages: e.toString() });
  }
}

async function sell(call, callback) {
  const ticker = call.request.ticker.toUpperCase();
  const coin = tradingCoins[ticker].baseAsset;

  try {
    const quantity = await getQuantity(coin);
    const soldQuantity = await getMaxSoldQuantity(ticker, quantity);
    const res = await instance.post(
      "/api/v3/order",
      qsWithSignature({
        symbol: ticker,
        side: "SELL",
        type: "MARKET",
        quantity: soldQuantity,
        timestamp: Date.now(),
      }),
    );

    const resMessages = mapOrders(coin, res.data.fills, "SOLD");
    sendNotification(resMessages);
    console.log(resMessages);

    callback(null, { messages: resMessages });
  } catch (e) {
    sendNotification(e.toString());
    console.log(e);
    callback(null, { messages: e.toString() });
  }
}

async function getQuantity(coin) {
  const res = await instance.get(
    "/api/v3/account?" + qsWithSignature({ timestamp: Date.now() }),
  );
  const asset = res.data["balances"].find((e) => e.asset === coin);

  if (!asset) {
    throw "No " + coin + " available in the spot wallet";
  }

  const quantity = asset["free"];
  return quantity;
}

async function getMaxSoldQuantity(ticker, quantity) {
  try {
    const res = await instance.get("/api/v3/exchangeInfo");
    res.data.symbols
      .filter((e) => e.symbol in tradingCoins)
      .forEach((e) => {
        tradingCoins[e.symbol] = e;
      });
  } catch (e) {
    console.log(e);
  }

  const stepSize = tradingCoins[ticker].filters.find(
    (e) => e.filterType === "LOT_SIZE",
  ).stepSize;
  const precision = countPrecision(stepSize);

  return (Math.floor(quantity / stepSize) * stepSize).toFixed(precision);
}

function countPrecision(stepSize) {
  const trimedStepSize = stepSize.toString();
  const len = trimedStepSize.length;

  return len - trimedStepSize.indexOf(".") - 1;
}

function qsWithSignature(params) {
  const qs = querystring.stringify(params);

  return qs + "&signature=" + hmacSha256(qs, process.env.SECRET_KEY);
}

function mapOrders(coin, orders, type) {
  return orders.map((e) => {
    return `${type} ${coin}, price ${e.price} at ${new Date(Date.now())}`;
  });
}

const server = new grpc.Server();
server.addService(executor.service, {
  buy,
  sell,
});
server.bindAsync(
  "0.0.0.0:50051",
  grpc.ServerCredentials.createInsecure(),
  () => {
    server.start();
  },
);

process.once("SIGTERM", gracefuleShutdown);
process.once("SIGINT", gracefuleShutdown);

function gracefuleShutdown() {
  sendNotification("Shutting down...");
  setTimeout(() => {
    process.exit();
  }, 1000);
}

function sendNotification(message) {
  request(
    {
      method: "POST",
      uri: NOTIFY_URL,
      header: {
        "Content-Type": "multiplart/form-data",
      },
      auth: {
        bearer: process.env.TOKEN,
      },
      form: {
        message: message,
      },
    },
    (err, _, body) => {
      if (err) {
        console.log(err);
      } else {
        console.log(body);
      }
    },
  );
}
